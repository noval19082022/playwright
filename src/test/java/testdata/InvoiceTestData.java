package testdata;

import lombok.Getter;
import lombok.Setter;

public class InvoiceTestData {
    @Setter @Getter
    private static String invoiceNumber;

    @Setter @Getter
    private static String manualInvoiceNumber;
}
