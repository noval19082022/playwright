package data.api;

import lombok.Getter;
import lombok.Setter;

public class OwnerLogin {
    @Setter @Getter
    private static String password, phoneNumber, token;
}
