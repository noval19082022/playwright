package pageobject.common;

import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;
import utilities.PlaywrightHelpers;

import java.util.List;

public class LoadingPO {
    private Page page;
    private PlaywrightHelpers playwright;

    private Locator loadingIcon;
    private Locator loadingAnimation;

    public LoadingPO(Page page) {
        this.page = page;
        this.playwright = new PlaywrightHelpers(this.page);
        loadingIcon = page.locator(".c-loader");
        loadingAnimation = page.getByTestId("loading-animation");
    }

    /**
     * Wait for loading icon disappear
     */
    public void waitForLoadingIconDisappear() {
        var waitDelay = 5000.0;
        var maxLoop = 10;
        if (playwright.waitTillLocatorIsVisible(loadingIcon.nth(0)) || playwright.waitTillLocatorIsVisible(loadingAnimation)) {
            List<Locator> loadingIconList = playwright.getLocators(loadingIcon);
            if (playwright.waitTillLocatorIsVisible(loadingIconList.get(0))) {
                for (Locator loadingIcon : loadingIconList) {
                    playwright.waitTillLocatorIsNotVisible(loadingIcon, waitDelay, maxLoop);
                }
            }
            if (playwright.waitTillLocatorIsVisible(loadingAnimation)) {
                playwright.waitTillLocatorIsNotVisible(loadingAnimation, waitDelay, maxLoop);
            }
        }
    }
}
